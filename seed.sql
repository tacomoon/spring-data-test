CREATE TABLE address (
    id         BIGSERIAL NOT NULL PRIMARY KEY,
    street     TEXT      NOT NULL,
    apartments INT       NOT NULL
);

CREATE TABLE users (
    id         BIGSERIAL   NOT NULL PRIMARY KEY,
    name       TEXT        NOT NULL,
    phone      VARCHAR(11) NOT NULL,
    address_id BIGINT      NULL REFERENCES address(id)
);