#!/usr/bin/env bash

VOLUME_NAME="spring-data-volume"
DATABASE_IMAGE="postgres:11.4"

docker rm "$(docker ps --all --quiet --filter ancestor=${DATABASE_IMAGE})"
docker volume rm ${VOLUME_NAME}