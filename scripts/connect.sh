#!/usr/bin/env bash

DATABASE_IMAGE="postgres:11.4"
CONTAINER_ID=$(docker ps --quiet --filter ancestor=${DATABASE_IMAGE})

docker exec -it "${CONTAINER_ID}" /bin/bash -c "psql -U postgres"