#!/usr/bin/env bash

VOLUME_NAME="spring-data-volume"
DATABASE_IMAGE="postgres:11.4"

if [[ -z $(docker volume ls -q -f name=${VOLUME_NAME}) ]]; then
  docker volume create ${VOLUME_NAME}
fi

if [[ -z $(docker images ${DATABASE_IMAGE} -q) ]]; then
  docker pull ${DATABASE_IMAGE}
fi

docker run -p 5433:5432 \
  --volume ${VOLUME_NAME}:/var/lib/postgresql/data \
  --volume "${PWD}"/../seed.sql:/docker-entrypoint-initdb.d/seed.sql \
  postgres:11.4
