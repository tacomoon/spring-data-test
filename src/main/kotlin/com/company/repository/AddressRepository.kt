package com.company.repository

import com.company.domain.Address
import com.company.domain.User
import org.springframework.data.repository.CrudRepository

/**
 * @author eugene.gorbachev
 * @since 2019-08-04
 */
interface AddressRepository : CrudRepository<Address, Long>
