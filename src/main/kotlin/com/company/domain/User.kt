package com.company.domain

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Reference
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

/**
 * @author eugene.gorbachev
 * @since 2019-08-04
 */
@Table("address")
data class Address(@Id val id: Long? = null,
                   val street: String,
                   val apartments: Int)

@Table("users")
data class User(@Id val id: Long? = null,
                val name: String,
                val phone: String,
//                @Reference(Address::class)
                @Column("id") val address: Address? = null)