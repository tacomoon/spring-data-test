package com.company

import com.company.domain.Address
import com.company.domain.User
import com.company.repository.AddressRepository
import com.company.repository.UserRepository
import org.postgresql.ds.PGSimpleDataSource
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories
import org.springframework.data.jdbc.repository.config.JdbcConfiguration
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import javax.sql.DataSource

/**
 * @author eugene.gorbachev
 * @since 2019-08-04
 */
@SpringBootApplication
class Application

@Configuration
@EnableJdbcRepositories
class CustomerConfig : JdbcConfiguration() {

    @Bean
    fun dataSource(): DataSource {
        return DataSourceBuilder.create()
                .type(PGSimpleDataSource::class.java)
                .driverClassName("org.postgresql.Driver")
                .url("jdbc:postgresql://localhost:5433/postgres")
                .username("postgres")
                .password("")
                .build()
    }

    @Bean
    fun operations(): NamedParameterJdbcOperations {
        return NamedParameterJdbcTemplate(dataSource())
    }
}

fun main(args: Array<String>) {
    val context = SpringApplication.run(Application::class.java, *args)

    val userRepository = context.getBean(UserRepository::class.java)
    val addressRepository = context.getBean(AddressRepository::class.java)

    with(userRepository) {
        save(User(name = "Andrey", phone = "1923", address = Address(street = "Lenin st.", apartments = 12)))
        save(User(name = "Eugene", phone = "678909"))
        save(User(name = "Dmitriy", phone = "567829"))
    }

    //        userRepository.save(new User(29L, "Fuck", "228"));

    println(userRepository.findAll())
}